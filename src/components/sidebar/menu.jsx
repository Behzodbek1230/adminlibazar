import React from "react";
import { useTranslation, withTranslation } from "react-i18next";

// function Menu() {
//   const { t } = useTranslation();
// function menus() {
const Menus = [
  {
    path: "/home",
    icon: "fa fa-th",
    title: "Dashboard",
  },
 

  {
    path: "/card",
    icon: "far fa-images",
    title: "Card",
  },

  {
    path: "/category",
    icon: "far fa-list-alt",
    title: "Category",
  },
  {
    path: "/banner",
    icon: "far fa-list-alt",
    title: "Banner",
  },
];
// return Menus;
// }
// return menus();
// }
export default Menus;
