import React, { useEffect, useState } from "react";
import { DownOutlined } from "@ant-design/icons";
import { Dropdown, Menu, Space } from "antd";
import { useTranslation, withTranslation } from "react-i18next";
import LanRu from "../../assets/icons/rus.svg";
import LanUz from "../../assets/icons/uzb.svg";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { langReducer } from "../../Redux/Reducers/Lang/langReducer";
function SelectLan() {
  const selectlan = useSelector((state) => state.langReducer);
  const { t, i18n } = useTranslation();
  const [Title, setTitle] = useState();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const dispatch = useDispatch();
  useEffect(() => {
    if (selectlan.value == "uz") {
      setTitle("O'zbek");
      i18n.changeLanguage("uz");
    }
    if (selectlan.value == "ru") {
      setTitle("Русский");
      i18n.changeLanguage("ru");
    }

  }, [selectlan.value]);

  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };



  const [visible, setVisible] = useState(false);

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      localStorage.setItem("lan", "uz");
      dispatch(langReducer("uz"));
      i18n.changeLanguage("uz");
      setTitle("O'zbek");
    }
    if (e.key === "2") {
      setTitle("Русский");
      localStorage.setItem("lan", "ru");
      dispatch(langReducer("ru"));
      i18n.changeLanguage("ru");
    }
  };

  const handleVisibleChange = (flag) => {
    setVisible(flag);
  };
  const menu = (
    <Menu
      onClick={handleMenuClick}
      items={[
        {
          label: "O'zbek",
          key: "1",
        },
        {
          label: "Русский",
          key: "2",
        },
      ]}
    />
  );
  return (
    <Dropdown
      overlay={menu}
      onVisibleChange={handleVisibleChange}
      visible={visible}
    >
      <div>
        <Space>
          {Title}
          {/* <DownOutlined /> */}
        </Space>
      </div>
    </Dropdown>
  );
}
export default withTranslation()(SelectLan);
