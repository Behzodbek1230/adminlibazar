import React, { useEffect } from "react";
import { useState } from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { langReducer } from "../../../Redux/Reducers/Lang/langReducer.js";
import { useTranslation, withTranslation } from "react-i18next";

export default function DropdownLanguage() {
  const selectlan = useSelector((state) => state.langReducer);
  const { t, i18n } = useTranslation();
  const [Title, setTitle] = useState();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [dropdownOpen, setdropdownOpen] = useState(false);
  useEffect(() => {
    if (selectlan.value == "uz") {
      setTitle("O'zbek");
      i18n.changeLanguage("uz");
    }
    if (selectlan.value == "ru") {
      setTitle("Русский");
      i18n.changeLanguage("ru");
    }
  }, [selectlan.value]);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const [visible, setVisible] = useState(false);

  const handleMenuClick = (e) => {
    if (e === "uz") {
      localStorage.setItem("lan", "uz");
      dispatch(langReducer("uz"));
      i18n.changeLanguage("uz");
      setTitle("O'zbek");
      setdropdownOpen(false);
    }
    if (e === "ru") {
      setTitle("Русский");
      localStorage.setItem("lan", "ru");
      dispatch(langReducer("ru"));
      i18n.changeLanguage("ru");
      setdropdownOpen(false);
    }
  };
  const handleVisibleChange = (flag) => {
    setVisible(flag);
  };
  return (
    <Dropdown
      isOpen={dropdownOpen}
      className="dropdown navbar-languager"
      tag="li"
      toggle={()=>console.log(dropdownOpen)}
    >
      <DropdownToggle
        className="dropdown-toggle"
        tag="a"
        onClick={() => setdropdownOpen(true)}
      >
        <span
          className={`flag-icon flag-icon-${selectlan.value} m-r-5`}
          title="us"
        ></span>
        <span className="name d-none d-sm-inline">{Title}</span>
      </DropdownToggle>
      <DropdownMenu className="dropdown-menu dropdown-menu-right" tag="ul">
        <DropdownItem onClick={() => handleMenuClick("ru")}>
          <span className="flag-icon flag-icon-ru m-r-5" title="ru"></span>{" "}
          Russion
        </DropdownItem>
        <DropdownItem onClick={() => handleMenuClick("uz")}>
          <span className="flag-icon flag-icon-uz m-r-5" title="uz"></span>{" "}
          Uzbek
        </DropdownItem>
      </DropdownMenu>
    </Dropdown>
  );
}
