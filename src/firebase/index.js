import firebase from 'firebase/compat/app';
import "firebase/compat/storage"
const firebaseConfig = {
  apiKey: "AIzaSyCuRzXjz-zhjrEik_DXnwOH43ePNHIdFl8",
  authDomain: "libazar.firebaseapp.com",
  projectId: "libazar",
  storageBucket: "libazar.appspot.com",
  messagingSenderId: "394370749833",
  appId: "1:394370749833:web:d8a8a2f949c4cd97c9fd58",
  measurementId: "G-HQTFBEHLK8"
};
firebase.initializeApp(firebaseConfig);
export const storageRef = firebase.storage();

export default firebase;