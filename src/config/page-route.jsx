import React from "react";
import { Redirect } from "react-router";
import HomeRoute from "../features/home/index.js";
import LoginRoute from "../features/logOut/Login.js";

import TableCard from "../features/Card/TableCard.js";
import AddCard from "../features/Card/add.js";
import TableCategory from "../features/Category/TableCategory.js";
import AddCategory from "../features/Category/add.js";
import Operators from "../features/Operators/Operators.jsx";
import AddOperator from "../features/Operators/AddOperator.jsx";
import Users from "../features/Users/Users.jsx";
import Sellers from "../features/Users/Sellers.jsx";

const routes = [
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/login" />,
  },
  {
    path: "/home",
    exact: true,
    title: "Home page",
    component: () => <HomeRoute />,
  },
  {
    path: "/addcard",
    exact: true,
    title: "Add card",
    component: () => <AddCard />,
  },

  {
    path: "/card",
    exact: true,
    title: "Cards",
    component: () => <TableCard />,
  },

  {
    path: "/addcategory",
    exact: true,
    title: "Add news",
    component: () => <AddCategory />,
  },

  {
    path: "/category",
    exact: true,
    title: "Category",
    component: () => <TableCategory />,
  },
  {
    path: "/operators",
    exact: true,
    title: "Operators",
    component: () => <Operators />,
  },
  {
    path: "/addoperator",
    exact: true,
    title: "Add operator",
    component: () => <AddOperator />,
  },
  {
    path: "/users",
    exact: true,
    title: "Users",
    component: () => <Users />,
  },
  {
    path: "/sellers",
    exact: true,
    title: "Sellers",
    component: () => <Sellers />,
  },

  {
    path: "/login",
    title: "Login",
    component: () => <LoginRoute />,
  },
];

export default routes;
