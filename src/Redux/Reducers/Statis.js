import { createSlice } from "@reduxjs/toolkit";
export const statisSlice = createSlice({
  name: "statis",
  initialState: {
    value: {},
  },
  reducers: {
    statisReducer: (state, action) => {
      state.value = action.payload;
    },
  },
});
export const { statisReducer } = statisSlice.actions;
export default statisSlice.reducer;
