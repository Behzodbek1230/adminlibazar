import { createSlice } from "@reduxjs/toolkit";
import url from "../../url.json";

const axios = require("axios");
const API_URL = url.url;

export const productSlide = createSlice({
  name: "product",
  initialState: {
    data: null,
  },
  reducers: {
    addproduct: (state, action) => {
      state.data.push(action.payload);
    },
    getproduct: (state, action) => {
      state.data = [action.payload];
    },
  },
});
axios.defaults.headers.common["Authorization"] = `Bearer ${localStorage.getItem(
  "token"
)}`;

export const getproductAsync = (data) => async (dispatch) => {
  try {
    const response = await axios.get(`${API_URL}/${data}`);
    dispatch(getproduct(response.data));
  } catch (err) {
    throw new Error(err);
  }
};
let a = [];
export const addproductAsync = (data) => async (dispatch) => {
  a.push(data);
  dispatch(addproduct(data))
  // try {
  //   // console.log(data);
  //   const response = await axios.post(API_URL, data);
  //   // console.log(response);
  //   dispatch(addproduct(a));
  // } catch (err) {
  //   throw new Error(err);
  // }
};

export const { addproduct, getproduct } = productSlide.actions;
export const showproduct = (state) => state.producte.data[0];
export default productSlide.reducer;
