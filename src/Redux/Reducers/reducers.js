import { createSlice } from "@reduxjs/toolkit";

export const listSlice = createSlice({
  name: "list",
  initialState: { value: [] },
  reducers: {
    seoReducer: (state, action) => {
      state.value = action.payload;
    },
  },
});
export const { seoReducer } = listSlice.actions;
export default listSlice.reducer;
