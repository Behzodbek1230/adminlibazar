import { createSlice } from "@reduxjs/toolkit";

export const newsSlice = createSlice({
  name: "news",
  initialState: { value: [] },
  reducers: {
    newsReducer: (state, action) => {
      state.value = action.payload;
    },
  },
});
export const { newsReducer } = newsSlice.actions;
export default newsSlice.reducer;
