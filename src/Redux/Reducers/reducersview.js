import { createSlice } from "@reduxjs/toolkit";

export const viewSlice = createSlice({
  name: "view",
  initialState: { value: {} },
  reducers: {
    seoViewReducer: (state, action) => {
      state.value = { ...action.payload };
    },
  },
});
export const { seoViewReducer } = viewSlice.actions;
export default viewSlice.reducer;
