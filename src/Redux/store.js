import SeoReducer from "./Reducers/reducers";
import viewReducer from "./Reducers/reducersview";
import Tokenreducer from "./Reducers/token";
import { configureStore } from "@reduxjs/toolkit";
import NewsReducer from "./Reducers/news";
import langReducer from "./Reducers/Lang/langReducer";
import productSlide from "./Reducers/product";
import statisReducer from "./Reducers/Statis";

export const store = configureStore({
  reducer: {
    news: NewsReducer,
    token: Tokenreducer,
    statistic: statisReducer,
    langReducer: langReducer,
    product: productSlide,
  },
});
