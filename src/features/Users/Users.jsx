import React from "react";
import namor from "namor";
import ReactTable from "react-table";
import { Link, Redirect } from "react-router-dom";
import { Panel, PanelHeader } from "../../components/panel/panel.jsx";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { withRouter } from "react-router-dom";
import url from "../../url.json";
import { Spin, Empty } from "antd";
import dateFormat from "dateformat";
import axios from "axios";
import { useTranslation, withTranslation } from "react-i18next";
import { toast } from "react-toastify";
import { statisSlice } from "../../Redux/Reducers/Statis.js";
class Users extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      ids: 0,
      sum: 0,
      total: 0,
      t: false,
      iddelite: 0,
      title: "",
      modalDialog: false,
      modalWithoutAnimation: false,
      modalMessage: false,
      modalAlert: false,
    };

    this.toggleModal = this.toggleModal.bind(this);
  }

  componentDidMount() {
    let sum = 0;
    let total = 0;
    axios({
      url: url.url + "auth/get-data",
    })
      .then((response) => {
        this.setState({ users: response.data.data });
        for (let index = 0; index < response.data?.data.length; index++) {
          total = total + response.data?.data[index].shareNumber;
          sum = sum + response.data?.data[index].successNumber;
        }
        localStorage.setItem("sum",sum)
        localStorage.setItem("total",total)
      })
      .catch(function (response) {
        console.log(response);
      });

    // dispatchEvent(statisReducer({total:this.state.total,sum:this.state.sum}))
  }

  toggleModal(name) {
    switch (name) {
      case "modalDialog":
        this.setState({ modalDialog: !this.state.modalDialog });
        break;
      case "modalWithoutAnimation":
        this.setState({
          modalWithoutAnimation: !this.state.modalWithoutAnimation,
        });
        break;
      case "modalMessage":
        this.setState({ modalMessage: !this.state.modalMessage });
        break;
      case "modalAlert":
        this.setState({ modalAlert: !this.state.modalAlert });
        break;
      default:
        break;
    }
  }
  handleadd(id) {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${localStorage.getItem("token")}`;
    const data = {};
    axios
      .put(url.url + `share/edit`, data)
      .then(function (response) {
        if (response.data.success) {
          // this.setState({ category: this.state.category.splice(this.state.ids, 1) });
          toast.success("Muaffaqiyatli o'chirildi!");
        }
      })
      .catch(function (response) {
        console.log(response);
      });
  }
  render() {
    const { t } = this.props;
    return (
      <div>
        {!localStorage.getItem("operator") ? (
          <ol className="breadcrumb float-xl-right">
            <li className="breadcrumb-item">
              <Link to="/home">{t("Home")}</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/card">{t("Products")}</Link>
            </li>{" "}
            <li className="breadcrumb-item">
              <Link to="/category">{t("Category")}</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/operators">{t("Operators")}</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/users">{t("Users")}</Link>
            </li>
          </ol>
        ) : (
          <ol className="breadcrumb float-xl-right">
            <li className="breadcrumb-item">
              <Link to="/home">{t("Home")}</Link>
            </li>

            <li className="breadcrumb-item">
              <Link to="/users">{t("Users")}</Link>
            </li>
          </ol>
        )}
        <h1 className="page-header">{t("Users")}</h1>
        <Modal
          isOpen={this.state.modalAlert}
          toggle={() => this.toggleModal("modalAlert")}
        >
          <ModalHeader toggle={() => this.toggleModal("modalAlert")}>
            {t("Add to ")}{" "}
          </ModalHeader>
          <ModalBody>
            <div className="alert alert-info m-b-0">
              <h5>
                <i className="fa fa-info-circle"></i> {this.state.title}
              </h5>
              <p></p>
            </div>
          </ModalBody>
          <ModalFooter>
            <button
              className="btn btn-white"
              onClick={() => this.toggleModal("modalAlert")}
            >
              {t("Cancel")}
            </button>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.toggleModal("modalAlert");
                this.handledelite();
              }}
            >
              {t("Add")}
            </button>
          </ModalFooter>
        </Modal>
        <Panel>
          <PanelHeader>{t("Users list")}</PanelHeader>

          <div className="panel-body">
            {this.state.users.length > 0 ? (
              <div className="table-responsive">
                <table className="table table-striped m-b-0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>{t("Username")}</th>
                      <th>{t("phoneNumber")}</th>

                      <th>{t("Created at")}</th>
                      <th>{t("shareNumber")}</th>

                      <th width="1%"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.users?.map((res, i) => (
                      <tr key={res.userName}>
                        <td className="with-img">{i + 1}</td>
                        <td>{res.userName}</td>
                        <td>{res.phoneNumber}</td>

                        <td>{dateFormat(res.createdAt, "dd.mm.yyyy")}</td>
                        <td>
                          {res.successNumber}/{res.shareNumber}
                        </td>

                        {localStorage.getItem("operator") ? (
                          <td className="with-btn">
                            <button
                              onClick={() => {
                                this.toggleModal("modalAlert");
                                this.setState({ iddelite: res.id });
                                this.setState({ title: res.username });
                                this.setState({ ids: i });
                              }}
                              className="btn btn-sm btn-primary "
                            >
                              <i className="fas fa-cart-plus"></i>
                            </button>
                          </td>
                        ) : (
                          ""
                        )}
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            ) : (
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  minHeight: 500,
                  backgroundColor: "white",
                  borderRadius: 12,
                }}
              >
                  <Empty description={t("No data")} />
              </div>
            )}
          </div>
        </Panel>
      </div>
    );
  }
}

export default withTranslation()(withRouter(Users));
