import React from "react";
import namor from "namor";
import ReactTable from "react-table";
import { Link, Redirect } from "react-router-dom";
import { Panel, PanelHeader } from "../../components/panel/panel.jsx";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { withRouter } from "react-router-dom";
import url from "../../url.json";
import { Spin, Empty } from "antd";
import dateFormat from "dateformat";
import axios from "axios";
import { useTranslation, withTranslation } from "react-i18next";
import { toast } from "react-toastify";
class Sellers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      ids: 0,
      t: false,
      iddelite: 0,
      title: "",
      modalDialog: false,
      modalWithoutAnimation: false,
      modalMessage: false,
      modalAlert: false,
    };

    this.toggleModal = this.toggleModal.bind(this);
  }

  componentDidMount() {
    axios({
      url: url.url + "share/get",
    })
      .then((response) => {
        this.setState({ users: response.data.data });
      })
      .catch(function (response) {
        console.log(response);
      });
  }

  toggleModal(name) {
    switch (name) {
      case "modalDialog":
        this.setState({ modalDialog: !this.state.modalDialog });
        break;
      case "modalWithoutAnimation":
        this.setState({
          modalWithoutAnimation: !this.state.modalWithoutAnimation,
        });
        break;
      case "modalMessage":
        this.setState({ modalMessage: !this.state.modalMessage });
        break;
      case "modalAlert":
        this.setState({ modalAlert: !this.state.modalAlert });
        break;
      default:
        break;
    }
  }
  handleadd() {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${localStorage.getItem("operator")}`;
    const data = {
      id: this.state.iddelite,
      statusCall: "ACCEPTED",
    };
    axios
      .put(url.url + `share/edit`, data)
      .then(function (response) {
        if (response.data.success) {
          // this.setState({ category: this.state.category.splice(this.state.ids, 1) });
          toast.success("Muaffaqiyatli qo'shildi!");
          window.location.reload();
        }
      })
      .catch(function (response) {
        console.log(response);
      });
  }
  handledel() {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${localStorage.getItem("operator")}`;
    const data = {
      id: this.state.iddelite,
      statusCall: "REJECTED",
    };
    axios
      .put(url.url + `share/edit`, data)
      .then(function (response) {
        if (response.data.success) {
          // this.setState({ category: this.state.category.splice(this.state.ids, 1) });
          toast.success("Muaffaqiyatli bekor qilindi!");
          window.location.reload();
        }
      })
      .catch(function (response) {
        console.log(response);
      });
  }
  render() {
    const { t } = this.props;
    return (
      <div>
        {!localStorage.getItem("operator") ? (
          <ol className="breadcrumb float-xl-right">
            <li className="breadcrumb-item">
              <Link to="/home">{t("Home")}</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/card">{t("Products")}</Link>
            </li>{" "}
            <li className="breadcrumb-item">
              <Link to="/category">{t("Category")}</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/operators">{t("Operators")}</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/users">{t("Users")}</Link>
            </li>
          </ol>
        ) : (
          <ol className="breadcrumb float-xl-right">
            <li className="breadcrumb-item">
              <Link to="/sellers">{t("Sellers")}</Link>
            </li>
          </ol>
        )}
        <h1 className="page-header">{t("Users")}</h1>
        <Modal
          isOpen={this.state.modalAlert}
          toggle={() => this.toggleModal("modalAlert")}
        >
          <ModalHeader toggle={() => this.toggleModal("modalAlert")}>
            {this.state.title}
          </ModalHeader>
          <ModalBody>
            <div className="alert alert-info m-b-0">
              <h5>
                <i className="fa fa-info-circle"></i>
                {this.state.ids} {t(" mahsulotni qabul qildimi?")}
              </h5>
              <p></p>
            </div>
          </ModalBody>
          <ModalFooter>
            {/* <button
              className="btn btn-white"
              onClick={() => this.toggleModal("modalAlert")}
            >
              {t("Chiqish")}
            </button> */}
            <button
              className="btn btn-danger"
              onClick={() => {
                this.toggleModal("modalAlert");
                this.handledel();
              }}
            >
              {t("Yo'q")}
            </button>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.toggleModal("modalAlert");
                this.handleadd();
              }}
            >
              {t("Ha")}
            </button>
          </ModalFooter>
        </Modal>
        <Panel>
          <PanelHeader>{t("Users list")}</PanelHeader>

          <div className="panel-body">
            {this.state.users.length > 0 ? (
              <div className="table-responsive">
                <table className="table table-striped m-b-0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>{t("sellerName")}</th>
                      <th>{t("By user")}</th>
                      <th>{t("phoneNumber")}</th>
                      <th>{t("Product")}</th>
                      <th>{t("Region")}</th>

                      <th>{t("Status")}</th>

                      <th>{t("Created at")}</th>

                      <th width="1%"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.users?.map((res, i) => (
                      <tr key={i}>
                        <td className="with-img">{i + 1}</td>
                        <td>{res.sellerName}</td>
                        <td>{res.userName}</td>
                        <td>
                          <a href={`tel:${res.phoneNumber}`}>
                            {res.phoneNumber}
                          </a>
                        </td>
                        <td>{res.productName}</td>
                        <td>{res.region}</td>

                        <td>
                          {res.statusCall == "REJECTED" ? (
                            <div className="btn btn-danger">
                              {t("Qabul qilinmadi")}{" "}
                            </div>
                          ) : res.statusCall == "ACCEPTED" ? (
                            <div className="btn btn-success">
                              {t("Qabul qilindi")}{" "}
                            </div>
                          ) : (
                            <div className="btn btn-secondary">
                              {t("Bog'lanilmadi")}{" "}
                            </div>
                          )}
                        </td>
                        <td>{dateFormat(res.createdAt, "dd.mm.yyyy ")}</td>

                        {localStorage.getItem("operator") ? (
                          <td className="with-btn">
                            <button
                              onClick={() => {
                                this.toggleModal("modalAlert");
                                this.setState({ iddelite: res.id });
                                this.setState({ title: res.product });
                                this.setState({ ids: res.sellerName });
                              }}
                              className="btn btn-sm btn-primary "
                            >
                              <i className="fas fa-cart-plus"></i>
                            </button>
                          </td>
                        ) : (
                          ""
                        )}
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            ) : (
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  minHeight: 500,
                  backgroundColor: "white",
                  borderRadius: 12,
                }}
              >
                <Spin tip={t("Loading...")}>
                  <Empty description={t("No data")} />
                </Spin>
              </div>
            )}
          </div>
        </Panel>
      </div>
    );
  }
}

export default withTranslation()(withRouter(Sellers));
