import React, { useEffect } from "react";
import { useState } from "react";
import { withTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { tokenReducer } from "../../Redux/Reducers/token";
import axios from "axios";
import url from "../../url.json";
function Home({ t }) {
  const dispatch = useDispatch();
  const [lenp, setlenp] = useState();
  const [leno, setleno] = useState();
  const [lena, setlena] = useState();
  const [sums, setsums] = useState();
  const [totals, settotals] = useState();
  useEffect(() => {
    dispatch(tokenReducer(localStorage.getItem("token")));
  });
  useEffect(() => {
    fetch(url.url + "product/get")
      .then((res) => res.json())
      .then((data) => {
        setlenp(data.data.length);
      });
    fetch(url.url + "auth/getAll?search=2")
      .then((res) => res.json())
      .then((data) => {
        setleno(data.data.length);
      });
    fetch(url.url + "auth/getAll?search=3")
      .then((res) => res.json())
      .then((data) => {
        setlena(data.data.length);
      });
    axios({
      url: url.url + "auth/get-data",
    })
      .then((response) => {
        let sum = 0;
        let total = 0;
        for (let index = 0; index < response.data?.data.length; index++) {
          total = total + response.data?.data[index].shareNumber;
          sum = sum + response.data?.data[index].successNumber;
        }
        setsums(sum)
        settotals(total)
      })
      .catch(function (response) {
        console.log(response);
      });
  }, []);
  return (
    <div>
      <ToastContainer />

      {!localStorage.getItem("operator") ? (
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link to="/home">{t("Home")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/card">{t("Products")}</Link>
          </li>{" "}
          <li className="breadcrumb-item">
            <Link to="/category">{t("Category")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/operators">{t("Operators")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/users">{t("Users")}</Link>
          </li>
        </ol>
      ) : (
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link to="/home">{t("Home")}</Link>
          </li>

          <li className="breadcrumb-item">
            <Link to="/sellers">{t("Sellers")}</Link>
          </li>
        </ol>
      )}
      <h1 className="page-header">{t("Dashboard")}</h1>

      <div className="row">
        <div className="col-xl-3 col-md-6">
          <div className="widget widget-stats bg-red">
            <div className="stats-icon">
              <i className="fas fa-th-list"></i>
            </div>
            <div className="stats-info">
              <h4>{t("Total products")}</h4>
              <p>{lenp}</p>
            </div>
            <div className="stats-link">
              <Link to="/card">
                View Detail <i className="fa fa-arrow-alt-circle-right"></i>
              </Link>
            </div>
          </div>
        </div>
        <div className="col-xl-3 col-md-6">
          <div className="widget widget-stats bg-orange">
            <div className="stats-icon">
              <i className="fas fa-cart-plus"></i>
            </div>
            <div className="stats-info">
              <h4>{t("Selling the product")}</h4>
              <p>
                {sums}/{totals}
              </p>
            </div>
            <div className="stats-link">
              <Link to="/dashboard/v1">
                View Detail <i className="fas fa-arrow-alt-circle-right"></i>
              </Link>
            </div>
          </div>
        </div>
        <div className="col-xl-3 col-md-6">
          <div className="widget widget-stats bg-grey-darker">
            <div className="stats-icon">
              <i className="fa fa-users"></i>
            </div>
            <div className="stats-info">
              <h4>{t("Users")}</h4>
              <p>{lena}</p>
            </div>
            <div className="stats-link">
              <Link to="/users">
                View Detail <i className="fa fa-arrow-alt-circle-right"></i>
              </Link>
            </div>
          </div>
        </div>
        <div className="col-xl-3 col-md-6">
          <div className="widget widget-stats bg-black-lighter">
            <div className="stats-icon">
              <i className="fa fa-users"></i>
            </div>
            <div className="stats-info">
              <h4>{t("Operators")}</h4>
              <p>{leno}</p>
            </div>
            <div className="stats-link">
              <Link to="/operators">
                View Detail <i className="fa fa-arrow-alt-circle-right"></i>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default withTranslation()(Home);
