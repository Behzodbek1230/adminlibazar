import React from "react";
import namor from "namor";
import ReactTable from "react-table";
import { Link, Redirect } from "react-router-dom";
import { Panel, PanelHeader } from "./../components/panel/panel.jsx";
import "react-table/react-table.css";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { withRouter } from "react-router-dom";
import { Input } from "reactstrap";
const range = (len) => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = () => {
  const statusChance = Math.random();
  return {
    firstName: namor.generate({ words: 1, numbers: 0 }),
    lastName: namor.generate({ words: 1, numbers: 0 }),
    age: Math.floor(Math.random() * 30),
    phone: Math.floor(Math.random() * 1000000),

    visits: Math.floor(Math.random() * 100),
    progress: Math.floor(Math.random() * 100),
    status:
      statusChance > 0.66
        ? "relationship"
        : statusChance > 0.33
        ? "complicated"
        : "single",
  };
};

function makeData(len = 5553) {
  return range(len).map((d) => {
    return {
      ...newPerson(),
      children: range(10).map(newPerson),
    };
  });
}

class TableData extends React.Component {
  constructor(props) {
    super(props);
    // this.handleSubmit = this.handleSubmit.bind(this);

    this.data = makeData();
    this.defaultSorted = [
      {
        id: "firstName",
        desc: false,
      },
    ];
    this.tableColumns = [
      {
        columns: [
          {
            Header: "Company",
            accessor: "firstName",
          },
          {
            Header: "Name",
            accessor: "firstName",
          },

          {
            Header: "Phone",
            accessor: "phone",
          },

          {
            Header: "Truck number",
            accessor: "age",
          },

          {
            Header: "CDL",
            accessor: "Cdl",
          },
          {
            Header: "Trailler number",
            accessor: "age",
          },
          {
            Header: "Trailler type",
            Cell: (row) => (
              <Input type="select" name="select" id="exampleSelect">
                <option>Trailler-1</option>
                <option>Trailler-2</option>
                <option>Trailler-3</option>
                <option>Trailler-4</option>
                <option>Trailler-5</option>
              </Input>
            ),
          },
          {
            Header: "Pl number",
            accessor: "age",
          },
          {
            Header: "Status",
            Cell: (row) => (
              <Input type="select" name="select" id="exampleSelect">
                <option>Select-1</option>
                <option>Select-2</option>
                <option>Select-3</option>
                <option>Select-4</option>
                <option>Select-5</option>
              </Input>
            ),
          },

          {
            width: 130,
            sortable: false,
            filterable: false,
            Cell: (cell) => (
              <div style={{ width: "200" }}>
                <button
                  className="btn btn-success"
                  style={{
                    marginLeft: "10px",
                  }}
                  type="button"
                  value={cell.accessor}
                  onClick={() => this.props.history.push("/add")}
                >
                  <i className="fas fa-pencil-alt fa-fw"></i>
                </button>
                <button
                  type="button"
                  onClick={() => this.toggleModal("modalAlert")}
                  className="btn btn-danger"
                  style={{
                    marginLeft: "20px",
                  }}
                >
                  <i className="fas fa-trash-alt"></i>
                </button>
              </div>
            ),
          },
        ],
      },
    ];
    this.state = {
      modalDialog: false,
      modalWithoutAnimation: false,
      modalMessage: false,
      modalAlert: false,
      sweetAlertPrimary: false,
      sweetAlertInfo: false,
      sweetAlertWarning: false,
      sweetAlertSuccess: false,
      sweetAlertError: false,
    };

    this.toggleModal = this.toggleModal.bind(this);
    this.toggleSweetAlert = this.toggleSweetAlert.bind(this);
    this.addNotification = this.addNotification.bind(this);
    this.notificationDOMRef = React.createRef();
  }

  addNotification(
    notificationType,
    notificationTitle,
    notificationMessage,
    notificationPosition,
    notificationContent
  ) {
    if (notificationContent) {
      notificationContent = (
        <div className="widget-list widget-list-rounded inverse-mode w-100">
          <div className="widget-list-item">
            <div className="widget-list-media">
              <img
                src="../assets/img/user/user-12.jpg"
                alt=""
                className="rounded"
              />
            </div>
            <div className="widget-list-content">
              <h4 className="widget-list-title">Christopher Struth</h4>
              <p className="widget-list-desc">Bank Transfer</p>
            </div>
          </div>
        </div>
      );
    }
  }

  toggleModal(name) {
    switch (name) {
      case "modalDialog":
        this.setState({ modalDialog: !this.state.modalDialog });
        break;
      case "modalWithoutAnimation":
        this.setState({
          modalWithoutAnimation: !this.state.modalWithoutAnimation,
        });
        break;
      case "modalMessage":
        this.setState({ modalMessage: !this.state.modalMessage });
        break;
      case "modalAlert":
        this.setState({ modalAlert: !this.state.modalAlert });
        break;
      default:
        break;
    }
  }

  toggleSweetAlert(name) {
    switch (name) {
      case "primary":
        this.setState({ sweetAlertPrimary: !this.state.sweetAlertPrimary });
        break;
      case "info":
        this.setState({ sweetAlertInfo: !this.state.sweetAlertInfo });
        break;
      case "success":
        this.setState({ sweetAlertSuccess: !this.state.sweetAlertSuccess });
        break;
      case "warning":
        this.setState({ sweetAlertWarning: !this.state.sweetAlertWarning });
        break;
      case "error":
        this.setState({ sweetAlertError: !this.state.sweetAlertError });
        break;
      default:
        break;
    }
  }

  render() {
    return (
      <div>
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link to="/home">Home</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/drivers">Tables</Link>
          </li>
        </ol>
        <h1 className="page-header">Drivers</h1>

        <Modal
          isOpen={this.state.modalAlert}
          toggle={() => this.toggleModal("modalAlert")}
        >
          <ModalHeader toggle={() => this.toggleModal("modalAlert")}>
            Alert Header
          </ModalHeader>
          <ModalBody>
            <div className="alert alert-danger m-b-0">
              <h5>
                <i className="fa fa-info-circle"></i> Alert Header
              </h5>
              <p>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                scelerisque ante sollicitudin commodo. Cras purus odio,
                vestibulum in vulputate at, tempus viverra turpis. Fusce
                condimentum nunc ac nisi vulputate fringilla. Donec lacinia
                congue felis in faucibus.
              </p>
            </div>
          </ModalBody>
          <ModalFooter>
            <button
              className="btn btn-white"
              onClick={() => this.toggleModal("modalAlert")}
            >
              Close
            </button>
            <button className="btn btn-danger">Action</button>
          </ModalFooter>
        </Modal>

        <Panel>
          <PanelHeader>Drivers Table</PanelHeader>
          <div style={{ textAlign: "right", marginRight: "10px" }}>
            <button
              className="btn btn-primary fileinput-button m-r-3"
              style={{
                marginTop: "5px",
                marginRight: "0px",
                marginLeft: "avto",
              }}
              onClick={() => this.props.history.push("/add")}
            >
              <i className="fa fa-fw fa-plus"></i>
              Add..
            </button>
          </div>
          <ReactTable
            filterable
            data={this.data}
            columns={this.tableColumns}
            defaultPageSize={10}
            defaultSorted={this.defaultSorted}
            className="-striped -highlight"
            style={{ overflow: "auto" }}
          />
        </Panel>
      </div>
    );
  }
}

export default withRouter(TableData);
