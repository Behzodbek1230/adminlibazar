import React from "react";
import namor from "namor";
import ReactTable from "react-table";
import { Link, Redirect } from "react-router-dom";
import { Panel, PanelHeader } from "../../components/panel/panel.jsx";
import { Spin, Empty } from "antd";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { withRouter } from "react-router-dom";
import "./Card.scss";
import url from "../../url.json";
import dateFormat, { masks } from "dateformat";
import { withTranslation } from "react-i18next";
import axios from "axios";
import { toast } from "react-toastify";
class TableCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      load: false,
      card: [],
      cards: [],
      title: "",
      iddelite: 0,
      ids: 0,
      modalDialog: false,
      modalWithoutAnimation: false,
      modalMessage: false,
      modalAlert: false,
    };

    this.toggleModal = this.toggleModal.bind(this);
  }

  componentDidMount() {
    fetch(url.url + "product/get")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ card: data.data });
      });
  }

  handledelite() {
    this.setState({ load: false });
    const delstate = this.state.card;
    delstate.splice(this.state.ids, 1);
    this.setState({ card: delstate });

    axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${localStorage.getItem("token")}`;

    axios
      .put(url.url + `product/disable/${this.state.iddelite}`)
      .then(function (response) {
        if (response.data.success) {
          // this.setState({ category: this.state.category.splice(this.state.ids, 1) });
          toast.success("Muaffaqiyatli o'chirildi!");
        }
      })
      .catch(function (response) {
        console.log(response);
      });
  }
  toggleModal(name) {
    switch (name) {
      case "modalDialog":
        this.setState({ modalDialog: !this.state.modalDialog });
        break;
      case "modalWithoutAnimation":
        this.setState({
          modalWithoutAnimation: !this.state.modalWithoutAnimation,
        });
        break;
      case "modalMessage":
        this.setState({ modalMessage: !this.state.modalMessage });
        break;
      case "modalAlert":
        this.setState({ modalAlert: !this.state.modalAlert });
        break;
      default:
        break;
    }
  }

  render() {
    const { t } = this.props;

    return (
      <div>
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link to="/home">{t("Home")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/card">{t("Products")}</Link>
          </li>{" "}
          <li className="breadcrumb-item">
            <Link to="/category">{t("Category")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/operators">{t("Operators")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/users">{t("Users")}</Link>
          </li>
        </ol>
        <h1 className="page-header">{t("Products")}</h1>

        <Modal
          isOpen={this.state.modalAlert}
          toggle={() => this.toggleModal("modalAlert")}
        >
          <ModalHeader toggle={() => this.toggleModal("modalAlert")}>
            {t("Delete")}{" "}
          </ModalHeader>
          <ModalBody>
            <div className="alert alert-danger m-b-0">
              <h5>
                <i className="fa fa-info-circle"></i> {this.state.title}
              </h5>
              <p></p>
            </div>
          </ModalBody>
          <ModalFooter>
            <button
              className="btn btn-white"
              onClick={() => this.toggleModal("modalAlert")}
            >
              {t("Cancel")}
            </button>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.toggleModal("modalAlert");
                this.handledelite();
              }}
            >
              {t("Delete")}
            </button>
          </ModalFooter>
        </Modal>

        <Panel>
          <PanelHeader>{t("Cards list")}</PanelHeader>
          <div style={{ textAlign: "right", marginRight: "10px" }}>
            <button
              className="btn btn-primary fileinput-button m-r-3"
              style={{
                marginTop: "5px",
                marginRight: "0px",
                marginLeft: "avto",
              }}
              onClick={() => this.props.history.push("/addcard")}
            >
              <i className="fa fa-fw fa-plus"></i>
              {t("Add")}..
            </button>
          </div>
          <div className="panel-body">
            {this.state.card.length > 0 ? (
              <div className="table-responsive">
                <table className="table table-striped m-b-0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>{t("Name")}</th>
                      <th>{t("description")}</th>
                      <th>{t("price")}</th>
                      <th>{t("category")}</th>
                      <th width="1%"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.card.map((res, index) => (
                      <tr key={index}>
                        <td className="with-img">
                          <img
                            src={res.imageUrls[0]}
                            className="img-rounded height-30"
                          />
                        </td>
                        <td>{res.name}</td>
                        <td>{res.description}</td>
                        <td>
                          {res.price} <span>{res.currency}</span>
                        </td>
                        <td>{res.categoryName}</td>

                        <td className="with-btn">
                          <div
                            onClick={() =>
                              this.props.history.push({
                                pathname: "/addcard",
                                search: `?id=${res.id}`,
                                // state: { detail: response.data },
                              })
                            }
                            className="btn btn-sm btn-primary  m-r-2"
                          >
                            <i className="fas fa-pencil-alt fa-fw"></i>
                          </div>
                          <button
                            onClick={() => {
                              this.toggleModal("modalAlert");

                              this.setState({ iddelite: res.id });
                              this.setState({ title: res.name });
                              this.setState({ ids: index });
                            }}
                            className="btn btn-sm btn-danger "
                          >
                            <i className="fas fa-trash-alt"></i>
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            ) : (
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  minHeight: 500,
                  backgroundColor: "white",
                  borderRadius: 12,
                }}
              >

                  <Empty description={t("No data")} />
                
              </div>
            )}
          </div>
        </Panel>
      </div>
    );
  }
}

export default withTranslation()(withRouter(TableCard));
