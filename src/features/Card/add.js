import React, { useEffect, useState } from "react";
import { Panel, PanelBody, PanelHeader } from "../../components/panel/panel";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useHistory, useLocation } from "react-router-dom";
import { Form, FormGroup, Label, Input, Row, Col } from "reactstrap";
import url from "../../url.json";
import FileUpload from "./components/Pictures";
import Standart from "./media";
import Picture from "./Picture";
import FileUploadd from "./components/file-upload/file-upload.component";
import { withTranslation } from "react-i18next";
import { storageRef } from "../../firebase";
import { Spin } from "antd";
import axios from "axios";
import { useDispatch } from "react-redux";
function AddCard({ t }) {
  const location = useLocation();
  const dispatch = useDispatch();
  const history = useHistory();
  const [resId, setresId] = useState();
  const [currenCat, setCurrenCat] = useState();
  const [mainImages, setmainImages] = useState({
    profileImages: [],
  });
  const [listImages, setlistImages] = useState({
    profileImages: [],
  });
  const [category, setCategory] = useState([]);
  const [data, setData] = useState();
  const [images, setImages] = useState();
  const [text, setText] = useState("");
  const [urls, setUrls] = useState();
  const [load, setLoad] = useState(false);
  const [load2, setLoad2] = useState(false);

  useEffect(() => {
    fetch(url.url + "category/get")
      .then((res) => res.json())
      .then((data) => setCategory(data.data))
      .catch((error) => console.log(error));
  }, []);
  useEffect(() => {
    if (location.search) {
      fetch(url.url + `product/get/${location.search.slice(4)}`)
        .then((response) => response.json())
        .then((data) => {
          setData(data.data);
          fetch(url.url + `category/get/${data.data.categoryId}`)
            .then((response) => response.json())
            .then((data) => setCurrenCat(data.data));
        });
    }
  }, []);
  function handleSubmit() {
    setLoad(true);
    const name = document.getElementById("name").value;
    const description = document.getElementById("des").value;
    const price = document.getElementById("price").value;
    const categoryId = document.getElementById("exampleSelect").value;
    const currency = document.getElementById("currency").value;
    const statusProduct =
      document.getElementById("status").value == ""
        ? "SIMPLE"
        : document.getElementById("status").value;
    const offPrice = document.getElementById("discount").value;
    const urlVideo = document.getElementById("video").value;
    axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer  ${localStorage.getItem("token")}`;
    if (!location.search) {
      setresId(location.search.slice(4));
      const data = {
        orderNumber: 0,
        statusProduct: statusProduct,
        offPrice: offPrice,
        name: name,
        description: description,
        price: price,
        categoryId: categoryId,
        currency: currency,
        urlVideo: urlVideo,
      };
      axios({
        method: "post",
        url: url.url + "product/add",
        data: data,
      })
        .then(function (response) {
          if (response.data.success) {
            toast.success("Saqlandi !");
            setresId(response.data.data);
          } else toast.error(response.data.data);
          setLoad(false);
        })
        .catch(function (response) {
          toast.error(response);
        });
    } else {
      const data = {
        id: location.search.slice(4),
        orderNumber: 0,
        statusProduct: statusProduct,
        offPrice: offPrice,
        name: name,
        description: description,
        price: price,
        categoryId: categoryId,
        currency: currency,
        urlVideo: urlVideo,
      };
      setresId(location.search.slice(4));
      axios({
        method: "put",
        url: url.url + "product/edit",
        data: data,
      })
        .then(function (response) {
          if (response.data.success) {
            toast.success("Saqlandi !");
          } else toast.error(response.data.data);
          setLoad(false);
        })
        .catch(function (response) {
          toast.error(response);
        });
    }
  }
  function handleSubmitimg() {
    setLoad2(true);
    const name = document.getElementById("name").value;
    const description = document.getElementById("des").value;
    const price = document.getElementById("price").value;
    const categoryId = document.getElementById("exampleSelect").value;
    const currency = document.getElementById("currency").value;
    const statusProduct =
      document.getElementById("status").value == ""
        ? "SIMPLE"
        : document.getElementById("status").value;
    const offPrice = document.getElementById("discount").value;
    const urlVideo = document
      .getElementById("video")
      .value.replace("shorts/", "watch?v=");
    axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer  ${localStorage.getItem("token")}`;
    if (!location.search) {
      setresId(location.search.slice(4));
      const data = {
        orderNumber: 0,
        statusProduct: statusProduct,
        offPrice: offPrice,
        name: name ? name : 0,
        description: description,
        price: price,
        categoryId: categoryId,
        currency: currency,
        urlVideo: urlVideo,
      };
      axios({
        method: "post",
        url: url.url + "product/add",
        data: data,
      })
        .then(function (response) {
          if (response.data.success) {
            if (response.data.data) {
              let arr = 0;
              const listimage = listImages.profileImages;
              for (let i = 0; i < listimage.length; i++) {
                const newImage = listimage[i];
                const uploadTask = storageRef
                  .ref(`images/${response.data.data}/${newImage.name}`)
                  .put(newImage);
                uploadTask.on(
                  "state_changed",
                  (snapshot) => {},
                  (error) => {
                    console.log(error);
                  },
                  async () => {
                    await storageRef
                      .ref(`images/${response.data.data}`)
                      .child(newImage.name)
                      .getDownloadURL()
                      .then((urls) => {
                        console.log(urls);
                        axios.defaults.headers.common[
                          "Authorization"
                        ] = `Bearer  ${localStorage.getItem("token")}`;
                        const data = {
                          id: response.data.data,
                          imageUrl: urls,
                        };

                        axios({
                          method: "post",
                          url: url.url + "product/set-image",
                          data: data,
                        })
                          .then(function (response) {
                            arr++;
                            if (arr == listimage.length) {
                              setLoad2(false);
                              toast.success(response.data.data);
                              setlistImages();
                              history.goBack();
                            }
                          })
                          .catch(function (response) {
                            console.log(response);
                          });
                      });
                  }
                );
              }
            }
          } else toast.error(response.data.data);
          setLoad(false);
        })
        .catch(function (response) {
          toast.error(response);
        });
    } else {
      const data = {
        id: location.search.slice(4),
        orderNumber: 0,
        statusProduct: statusProduct,
        offPrice: offPrice,
        name: name ? name : 0,
        description: description,
        price: price,
        categoryId: categoryId,
        currency: currency,
        urlVideo: urlVideo,
      };
      setresId(location.search.slice(4));
      axios({
        method: "put",
        url: url.url + "product/edit",
        data: data,
      })
        .then(function (response) {
          if (response.data.success) {
            if (response.data.data && listImages.profileImages.length > 0) {
              let arr = 0;
              const listimage = listImages.profileImages;
              for (let i = 0; i < listimage.length; i++) {
                const newImage = listimage[i];
                const uploadTask = storageRef
                  .ref(`images/${newImage.name}`)
                  .put(newImage);
                uploadTask.on(
                  "state_changed",
                  (snapshot) => {},
                  (error) => {
                    console.log(error);
                  },
                  async () => {
                    await storageRef
                      .ref("images")
                      .child(newImage.name)
                      .getDownloadURL()
                      .then((urls) => {
                        axios.defaults.headers.common[
                          "Authorization"
                        ] = `Bearer  ${localStorage.getItem("token")}`;
                        const data = {
                          id: response.data.data,
                          imageUrl: urls,
                        };

                        axios({
                          method: "post",
                          url: url.url + "product/set-image",
                          data: data,
                        })
                          .then(function (response) {
                            arr++;
                            if (arr == listimage.length) {
                              setLoad2(false);
                              toast.success(response.data.data);
                              history.goBack();
                            }
                          })
                          .catch(function (response) {
                            console.log(response);
                          });
                      });
                  }
                );
              }
            } else {
              setLoad(false);
              history.goBack();
            }
          } else toast.error(response.data.data);
          setLoad(false);
        })
        .catch(function (response) {
          toast.error(response);
        });
    }
  }

  const updateUploadedFiles = (files) => {
    setmainImages({ ...mainImages, profileImages: files });
  };
  const updateUploadedFilesmul = (files) => {
    setlistImages({ ...listImages, profileImages: files });
  };
  const handleSubmitdiv = (event) => {
    event.preventDefault();
  };
  const onEditorChange = (event) => {
    let data = event.editor.getData();
    setText(data);
  };
  return (
    <Panel>
      <PanelHeader>{t("Product add")}</PanelHeader>
      <PanelBody>
        <Form>
          <Row>
            <Col>
              <FormGroup>
                <Label>{t("Name")}</Label>
                <Input
                  name="name"
                  id="name"
                  placeholder={t("Name")}
                  defaultValue={data?.name}
                />
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Label>{t("Category")}</Label>
                <Input type="select" name="select" id="exampleSelect">
                  {location.search ? (
                    <option value={data?.categoryId}>{currenCat?.name}</option>
                  ) : (
                    ""
                  )}
                  {category.map((res, i) => (
                    <option
                      key={i}
                      value={res.id}
                      defaultValue={data?.categoryId}
                    >
                      {res.name}
                    </option>
                  ))}
                </Input>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={7}>
              <FormGroup>
                <Label>{t("Video link")}</Label>
                <Input
                  name="video"
                  id="video"
                  type="text"
                  defaultValue={data?.urlVideo}
                />
              </FormGroup>
            </Col>

            <Col md={2}>
              <FormGroup>
                <Label>{t("Price")}</Label>
                <Input
                  name="price"
                  id="price"
                  type="number"
                  defaultValue={data?.price}
                />
              </FormGroup>
            </Col>
            <Col md={1}>
              <FormGroup>
                <Label>{t("currency")}</Label>
                <Input name="currency" id="currency" type="select">
                  <option value={"USD"}>USD</option>
                  <option value={"UZS"}>UZS</option>
                </Input>
              </FormGroup>
            </Col>
            <Col md={1}>
              <FormGroup>
                <Label>{t("status")}</Label>
                <Input name="status" id="status" type="select">
                  <option valu={"SIMPLE"}></option>

                  <option value={"NEW"}>New</option>
                  <option value={"OFF"}>OFF</option>
                </Input>
              </FormGroup>
            </Col>
            <Col md={1}>
              <FormGroup>
                <Label>{t("Chegirma")}</Label>
                <Input name="discount" id="discount" type="number" />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Label>{t("Descriptions")}</Label>
                <Input
                  defaultValue={data?.description}
                  name="des"
                  id="des"
                  type="textarea"
                  style={{ height: "150px" }}
                />
                {/* <CKEditor onChange={(e) => onEditorChange(e)} /> */}
              </FormGroup>
            </Col>
          </Row>
        </Form>
        <form onSubmit={handleSubmit}>
          <FileUploadd
            accept="*"
            label="Profile Image(s)"
            multiple
            updateFilesCb={updateUploadedFilesmul}
          />
        </form>
        <button
          className="landing-header__btn signup"
          style={{
            marginLeft: "15px",
            marginTop: "20px",
            paddingLeft: "25px",
            marginRight: "10px",
            paddingRight: "25px",
          }}
          type="button"
          onClick={handleSubmitimg}
        >
          {load2 ? <Spin size="small" className="mr-2" /> : ""}
          {t("Save")}
        </button>
      </PanelBody>
    </Panel>
  );
}
export default withTranslation()(AddCard);
