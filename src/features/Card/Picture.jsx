import React, { useState } from "react";
import { Form } from "reactstrap";
import FileUpload from "./components/Pictures.jsx";

function Picture() {
  const [newUserInfo, setNewUserInfo] = useState({
    profileImages: [],
  });

  const updateUploadedFiles = (files) =>
    setNewUserInfo({ ...newUserInfo, profileImages: files });
  const handleSubmit = (event) => {
    event.preventDefault();
    //logic to create new user...
  };

  return (
    <div>
      <div onSubmit={handleSubmit}>
        Tree picture
        <FileUpload
          accept=".jpg,.png,.jpeg"
          label="Profile Image(s)"
          updateFilesCb={updateUploadedFiles}
        />
      </div>
    </div>
  );
}

export default Picture;
