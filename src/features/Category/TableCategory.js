import React from "react";
import namor from "namor";
import ReactTable from "react-table";
import { Link, Redirect } from "react-router-dom";
import { Panel, PanelHeader } from "../../components/panel/panel.jsx";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { withRouter } from "react-router-dom";
import "./Card.scss";
import Img4 from "../../assets/css/default/images/cover-sidebar.jpg";
import Img2 from "../../assets/css/default/images/coming-soon.jpg";
import url from "../../url.json";
import { useTranslation, withTranslation } from "react-i18next";
import { toast } from "react-toastify";
import axios from "axios";
import { Spin, Empty, Switch } from "antd";
import dateFormat from "dateformat";
class TableCategory extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      category: [],
      categorys: [],
      load: false,
      ids: 0,
      t: false,
      iddelite: 0,
      title: "",
      modalDialog: false,
      modalWithoutAnimation: false,
      modalMessage: false,
      modalAlert: false,
    };

    this.toggleModal = this.toggleModal.bind(this);
  }
  componentDidMount() {
    axios({
      url: url.url + "category/get",
    })
      .then((response) => {
        if (response.data.success) {
          this.setState({ category: response.data.data });
        } else toast.error(response.data.data);
      })
      .catch(function (response) {
        console.log(response);
      });
  }
  handledelite() {
    this.setState({ load: false });
    const delstate = this.state.category;
    delstate.splice(this.state.ids, 1);
    this.setState({ category: delstate });

    axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${localStorage.getItem("token")}`;

    axios
      .put(url.url + `category/disable/${this.state.iddelite}`)
      .then(function (response) {
        if (response.data.success) {
          // this.setState({ category: this.state.category.splice(this.state.ids, 1) });
          toast.success("Muaffaqiyatli o'chirildi!");
        }
      })
      .catch(function (response) {
        console.log(response);
      });
  }
  toggleModal(name) {
    switch (name) {
      case "modalDialog":
        this.setState({ modalDialog: !this.state.modalDialog });
        break;
      case "modalWithoutAnimation":
        this.setState({
          modalWithoutAnimation: !this.state.modalWithoutAnimation,
        });
        break;
      case "modalMessage":
        this.setState({ modalMessage: !this.state.modalMessage });
        break;
      case "modalAlert":
        this.setState({ modalAlert: !this.state.modalAlert });
        break;
      default:
        break;
    }
  }
  onChange = (checked) => {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${localStorage.getItem("token")}`;

    axios
      .put(url.url + `category/disable/${checked}`)
      .then(function (response) {
        if (response.data.success) {
          // this.setState({ category: this.state.category.splice(this.state.ids, 1) });
          toast.success("Muaffaqiyatli o'chirildi!");
        }
      })
      .catch(function (response) {
        console.log(response);
      });
  };
  render() {
    const { t } = this.props;
    return (
      <div>
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link to="/home">{t("Home")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/card">{t("Products")}</Link>
          </li>{" "}
          <li className="breadcrumb-item">
            <Link to="/category">{t("Category")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/operators">{t("Operators")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/users">{t("Users")}</Link>
          </li>
        </ol>
        <h1 className="page-header">{t("Category")}</h1>

        <Modal
          isOpen={this.state.modalAlert}
          toggle={() => this.toggleModal("modalAlert")}
        >
          <ModalHeader toggle={() => this.toggleModal("modalAlert")}>
            {t("Delete")}
          </ModalHeader>
          <ModalBody>
            <div className="alert alert-danger m-b-0">
              <h5>
                <i className="fa fa-info-circle"></i> {this.state.title}
              </h5>
              <p></p>
            </div>
          </ModalBody>
          <ModalFooter>
            <button
              className="btn btn-white"
              onClick={() => this.toggleModal("modalAlert")}
            >
              {t("Cancel")}
            </button>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.toggleModal("modalAlert");
                this.handledelite();
              }}
            >
              {t("Delete")}
            </button>
          </ModalFooter>
        </Modal>

        <Panel>
          <PanelHeader>{t("Category list")}</PanelHeader>
          <div style={{ textAlign: "right", marginRight: "10px" }}>
            <button
              className="btn btn-primary fileinput-button m-r-3"
              style={{
                marginTop: "5px",
                marginRight: "0px",
                marginLeft: "avto",
              }}
              onClick={() => this.props.history.push("/addcategory")}
            >
              <i className="fa fa-fw fa-plus"></i>
              {t("Add")}..
            </button>
          </div>
          {/* <div className="blog__card"> */}
          <div className="panel-body">
            {this.state.category.length > 0 ? (
              <div className="table-responsive">
                <table className="table table-striped ">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>{t("Name")}</th>
                      <th>{t("Descriptions")}</th>
                      <th>{t("Created at")}</th>

                      <th width="1%"></th>
                    </tr>
                  </thead>

                  <tbody>
                    {this.state.category?.map((res, i) => (
                      <tr key={i}>
                        <td className="with-img">
                          <img
                            src={res.imageUrl}
                            className="img-rounded height-30"
                          />
                        </td>
                        <td>{res.name}</td>
                        <td
                          dangerouslySetInnerHTML={{ __html: res.description }}
                        ></td>
                        <td>{dateFormat(res.createdAt, "dd.mm.yyyy")}</td>

                        <td className="with-btn">
                          <div
                            onClick={() =>
                              this.props.history.push({
                                pathname: "/addcategory",
                                search: `id=${res.id}`,
                                // state: { detail: response.data },
                              })
                            }
                            className="btn btn-sm btn-primary  m-r-5"
                          >
                            <i className="fas fa-pencil-alt fa-fw"></i>
                          </div>
                          <button
                            onClick={() => {
                              this.toggleModal("modalAlert");
                              this.setState({ iddelite: res.id });
                              this.setState({ title: res.name });
                              this.setState({ ids: i });
                            }}
                            className="btn btn-sm btn-danger m-r-5"
                          >
                            <i className="fas fa-trash-alt"></i>
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            ) : (
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  minHeight: 500,
                  backgroundColor: "white",
                  borderRadius: 12,
                }}
              >
                <Spin tip={t("Loading...")}>
                  <Empty description={t("No data")} />
                </Spin>
              </div>
            )}
          </div>
        </Panel>
      </div>
    );
  }
}
export default withTranslation()(withRouter(TableCategory));
