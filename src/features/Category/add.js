import { CKEditor } from "ckeditor4-react";
import React, { useEffect, useState } from "react";
import { Panel, PanelBody, PanelHeader } from "../../components/panel/panel";
import { useHistory, useLocation } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Form, FormGroup, Label, Input, Row, Col } from "reactstrap";
import { Spin } from "antd";
import url from "../../url.json";
import FileUpload from "./components/Pictures";
import { withTranslation } from "react-i18next";
import axios from "axios";
import { firestore, storageRef } from "../../firebase";
function AddCategory({ t }) {
  const [load, setLoad] = useState(false);
  const [mainImages, setmainImages] = useState({
    profileImages: [],
  });
  const location = useLocation();
  const [datas, setDatas] = useState();
  const [imageUrl, setImageUrl] = useState();
  const [text, setText] = useState();
  const history = useHistory();
  const updateUploadedFiles = (files) => {
    setmainImages({ ...mainImages, profileImages: files });
  };

  const handleSubmitdiv = (event) => {
    event.preventDefault();
  };
  const onEditorChange = (event) => {
    let data = event.editor.getData();
    setText(data);
  };
  useEffect(() => {
    if (location.search) {
      fetch(url.url + `category/get/${location.search.slice(4)}`)
        .then((response) => response.json())
        .then((data) => setDatas(data.data));
    }
  }, []);
  function handleSubmit() {
    setLoad(true);
    const name = document.getElementById("name").value;
    const description = document.getElementById("des").value;
    const orderNumber = document.getElementById("order_number").value;
    const formData = new FormData();
    const mainPicture = mainImages?.profileImages[0];
    const uploadTask = storageRef
      .ref(`images/${mainPicture.name}`)
      .put(mainPicture);
    uploadTask.on(
      "state_changed",
      (snapshot) => {},
      (error) => {
        console.log(error);
      },
      () => {
        storageRef
          .ref("images")
          .child(mainPicture.name)
          .getDownloadURL()
          .then((urls) => {
            setImageUrl(urls);

            // console.log(category.id);

            axios.defaults.headers.common[
              "Authorization"
            ] = `Bearer  ${localStorage.getItem("token")}`;
            if (!location.search) {
              const category = {
                name: name,
                description: description,
                orderNumber: orderNumber,
                imageUrl: urls,
              };
              axios({
                method: "post",
                url: url.url + "category/add",
                data: category,
              })
                .then(function (response) {
                  if (response.data.success) {
                    toast.success(response.data.data);
                    setTimeout(() => {
                      setLoad(false);
                      history.goBack();
                    }, 1000);

                    setTimeout(() => {
                      setLoad(false);
                    }, 1000);
                  } else {
                    toast.error(response.data.message);
                    setLoad(false);
                  }
                })
                .catch(function (response) {
                  console.log(response);
                });
            } else {
              const category = {
                id: location.search.slice(4),
                name: name,
                description: description,
                orderNumber: orderNumber,
                imageUrl: urls,
              };
              axios({
                method: "put",
                url: url.url + "category/edit",
                data: category,
              })
                .then(function (response) {
                  toast.success(response.data.message);
                  setLoad(false);
                  history.goBack();
                })
                .catch(function (response) {
                  console.log(response);
                });
            }
          });
      }
    );
  }

  return (
    <Panel>
      <PanelHeader>{t("Category")}</PanelHeader>
      <PanelBody>
        <Form>
          <Row>
            <Col>
              <FormGroup>
                <Label>{t("Name")}</Label>
                <Input
                  name="name"
                  defaultValue={datas?.name}
                  id="name"
                  placeholder={t("Name")}
                />
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Label>{t("order_number")}</Label>
                <Input
                  name="order_number"
                  id="order_number"
                  type="number"
                  defaultValue={datas?.orderNumber}
                  placeholder={t("order_number")}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Label>{t("Descriptions")}</Label>
                {/* <CKEditor onChange={(e) => onEditorChange(e)} /> */}

                <Input
                  name="des"
                  id="des"
                  type="textarea"
                  style={{ height: "150px" }}
                  defaultValue={datas?.description}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col className="col-md-3">
              <div onSubmit={handleSubmitdiv}>
                <FileUpload
                  accept="*"
                  label="Profile Image(s)"
                  updateFilesCb={updateUploadedFiles}
                />
              </div>
            </Col>
          </Row>
        </Form>

        <button
          className="landing-header__btn signup"
          style={{
            marginLeft: "15px",
            marginTop: "20px",
            paddingLeft: "25px",
            marginRight: "10px",
            paddingRight: "25px",
          }}
          type="button"
          onClick={handleSubmit}
        >
          {load ? <Spin size="small" className="mr-2" /> : ""}
          {t("Save")}
        </button>
      </PanelBody>
    </Panel>
  );
}
export default withTranslation()(AddCategory);
