import React, { useState } from "react";
import FileUpload from "./components/Pictures.jsx";

function Picture() {
  const [newUserInfo, setNewUserInfo] = useState({
    profileImages: [],
  });

  const updateUploadedFiles = (files) =>
    setNewUserInfo({ ...newUserInfo, profileImages: files });
  const handleSubmit = (event) => {
    event.preventDefault();
    //logic to create new user...
  };

  return (
    <div>
      <div onSubmit={handleSubmit}>
        Banner picture
        <FileUpload
          accept="*"
          label="Profile Image(s)"
          updateFilesCb={updateUploadedFiles}
        />
      </div>
    </div>
  );
}

export default Picture;
