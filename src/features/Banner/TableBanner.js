import React from "react";
import namor from "namor";
import ReactTable from "react-table";
import { Link, Redirect } from "react-router-dom";
import { Panel, PanelHeader } from "../../components/panel/panel.jsx";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { withRouter } from "react-router-dom";
import "./Card.scss";
import url from "../../url.json";

class TableCategory extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      banner: [],
      banners: [],
      t: false,
      ids: 0,
      iddelite: 0,
      modalDialog: false,
      modalWithoutAnimation: false,
      modalMessage: false,
      modalAlert: false,
    };

    this.toggleModal = this.toggleModal.bind(this);
  }
  componentDidMount() {
    fetch(url.url + "banner")
      //.then((res) => res.json())
      .then((data) => this.setState({ banner: data.data }));
  }
  handledelite() {
    const id = {
      bannerId: this.state.iddelite,
    };
    this.setState({
      banners: this.state.banner.splice(this.state.ids - 1, 1),
    });

    fetch(url.url + "banner", {
      method: "DELETE",
      headers: {
        token: localStorage.getItem("token"),
        "Content-Type": "application/json;charset=utf-8",
      },
      body: JSON.stringify(id),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.status == 201) {
          this.setState({ t: true });
        }
      });
  }
  toggleModal(name) {
    switch (name) {
      case "modalDialog":
        this.setState({ modalDialog: !this.state.modalDialog });
        break;
      case "modalWithoutAnimation":
        this.setState({
          modalWithoutAnimation: !this.state.modalWithoutAnimation,
        });
        break;
      case "modalMessage":
        this.setState({ modalMessage: !this.state.modalMessage });
        break;
      case "modalAlert":
        this.setState({ modalAlert: !this.state.modalAlert });
        break;
      default:
        break;
    }
  }

  render() {
    return (
      <div>
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link to="/home">Home</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/news">Tables</Link>
          </li>
        </ol>
        <h1 className="page-header">Banner</h1>

        <Modal
          isOpen={this.state.modalAlert}
          toggle={() => this.toggleModal("modalAlert")}
        >
          <ModalHeader toggle={() => this.toggleModal("modalAlert")}>
            Delete
          </ModalHeader>
          <ModalBody>
            <div className="alert alert-danger m-b-0">
              <h5>
                <i className="fa fa-info-circle"></i> Do you want to delete the
                banner?
              </h5>
              <p></p>
            </div>
          </ModalBody>
          <ModalFooter>
            <button
              className="btn btn-white"
              onClick={() => this.toggleModal("modalAlert")}
            >
              Close
            </button>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.toggleModal("modalAlert");
                this.handledelite();
              }}
            >
              Delete
            </button>
          </ModalFooter>
        </Modal>

        <Panel>
          <PanelHeader>Banner list</PanelHeader>
          <div style={{ textAlign: "right", marginRight: "10px" }}>
            <button
              className="btn btn-primary fileinput-button m-r-3"
              style={{
                marginTop: "5px",
                marginRight: "0px",
                marginLeft: "avto",
              }}
              onClick={() => this.props.history.push("/addbanner")}
            >
              <i className="fa fa-fw fa-plus"></i>
              Add..
            </button>
          </div>
          <div className="cardd">
            {this.state.t
              ? this.state.banners.map((res, i) => (
                  <div className="card__box box-cardd" key={i}>
                    <div className="box-card__head head-card">
                      <img
                        className="head-card__img"
                        src={url.url + res.image}
                        alt="card img"
                        width="300"
                        height="350"
                      />
                    </div>
                    <div className="box-card__body body-card">
                      <div>
                        <button
                          className="btn btn-success"
                          style={{
                            marginLeft: "10px",
                          }}
                          type="button"
                          onClick={() => this.props.history.push("/addbanner")}
                        >
                          <i className="fas fa-pencil-alt fa-fw"></i>
                        </button>
                        <button
                          type="button"
                          onClick={() => {
                            this.toggleModal("modalAlert");
                            this.setState({ iddelite: res.banner_id });
                            this.setState({ ids: i + 1 });
                          }}
                          className="btn btn-danger"
                          style={{
                            marginLeft: "20px",
                          }}
                        >
                          <i className="fas fa-trash-alt"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                ))
              : this.state.banner.map((res, i) => (
                  <div className="card__box box-cardd" key={i}>
                    <div className="box-card__head head-card">
                      <img
                        className="head-card__img"
                        src={url.url + res.image}
                        alt="card img"
                        width="300"
                        height="350"
                      />
                    </div>
                    <div className="box-card__body body-card">
                      <div>
                        <button
                          className="btn btn-success"
                          style={{
                            marginLeft: "10px",
                          }}
                          type="button"
                          onClick={() => this.props.history.push("/addbanner")}
                        >
                          <i className="fas fa-pencil-alt fa-fw"></i>
                        </button>
                        <button
                          type="button"
                          onClick={() => {
                            this.toggleModal("modalAlert");
                            this.setState({ iddelite: res.banner_id });
                            this.setState({ ids: i + 1 });
                          }}
                          className="btn btn-danger"
                          style={{
                            marginLeft: "20px",
                          }}
                        >
                          <i className="fas fa-trash-alt"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                ))}
          </div>
        </Panel>
      </div>
    );
  }
}

export default withRouter(TableCategory);
