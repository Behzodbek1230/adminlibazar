import React, { useEffect, useState } from "react";
import { Panel, PanelBody, PanelHeader } from "../../components/panel/panel";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useHistory } from "react-router-dom";
import { Form, FormGroup, Label, Input, Row, Col } from "reactstrap";

import url from "../../url.json";
import FileUpload from "./components/Pictures";
import { useSelector } from "react-redux";

export default function AddBanner() {
  const history = useHistory();
  const [newUserInfo, setNewUserInfo] = useState({
    profileImages: [],
  });

  function handleSubmit() {
    const img = newUserInfo.profileImages[0];
    const formData = new FormData();

    formData.append("bannerImage", img);
    fetch(url.url + "banner", {
      method: "post",
      headers: { token: localStorage.getItem("token") },
      body: formData,
    })
      //.then((res) => res.json())
      .then((data) => {
        if (data.status == 201) {
          toast.success("Success!");
          setTimeout(() => {
            history.push("/banner");
          }, 2000);
        } else {
          toast.error("Server error!");
        }
      });
  }

  const updateUploadedFiles = (files) => {
    setNewUserInfo({ ...newUserInfo, profileImages: files });
  };
  const handleSubmitdiv = (event) => {
    event.preventDefault();
  };

  return (
    <Panel>
      <ToastContainer style={{ zIndex: "99999999999999", marginTop: "10px" }} />

      <PanelHeader>Banner</PanelHeader>
      <PanelBody>
        <Form>
          <div onSubmit={handleSubmitdiv}>
            News picture
            <FileUpload
              accept=".jpg,.png,.jpeg"
              label="Profile Image(s)"
              updateFilesCb={updateUploadedFiles}
            />
          </div>
        </Form>

        <button
          className="btn btn-success"
          style={{
            marginLeft: "5px",
            marginTop: "20px",

            paddingLeft: "25px",
            marginRight: "10px",
            paddingRight: "25px",
          }}
          type="button"
          onClick={() => handleSubmit()}
        >
          Save
        </button>
      </PanelBody>
    </Panel>
  );
}
