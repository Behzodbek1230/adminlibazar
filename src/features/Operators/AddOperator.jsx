import React, { useEffect, useState } from "react";
import { Panel, PanelBody, PanelHeader } from "../../components/panel/panel";
import { CKEditor } from "ckeditor4-react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useHistory } from "react-router-dom";
import { Form, FormGroup, Label, Input, Row, Col } from "reactstrap";
import axios from "axios";
import url from "../../url.json";
import { withTranslation } from "react-i18next";
import { Spin } from "antd";

function AddOperator({ t }) {
  const [load, setLoad] = useState(false);
  const history = useHistory();
  function handleSubmit() {
    setLoad(true);
    const fullname = document.getElementById("name").value;
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    const data = {
      fullName: fullname,
      userName: username,
      password: password,
      phoneNumber:""
    };

    axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer  ${localStorage.getItem("token")}`;
    axios({
      method: "post",
      url: url.url + "auth/add-operator",
      data: data,
    })
      .then(function (response) {
        if (response.data.success) {
          toast.success(t("Successfully added operator"));
          setTimeout(() => {
            history.goBack()
            setLoad(false);
          }, 1000);
        } else {
          toast.error(response.data.message);
          setLoad(false);
        }
      })
      .catch(function (response) {
        console.log(response);
      });
  }

  return (
    <Panel>
      <PanelHeader>{t("Add Operator")}</PanelHeader>
      <PanelBody>
        <Form>
          <Row>
            <Col>
              <FormGroup>
                <Label>{t("Name")}</Label>
                <Input name="name" id="name" placeholder={t("Name")} />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Label>{t("Username")}</Label>
                <Input
                  name="username"
                  id="username"
                  placeholder={t("Username")}
                />
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Label>{t("Password")}</Label>
                <Input name="password" id="password" type="password" />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <button
              className="landing-header__btn signup"
              style={{
                marginLeft: "15px",
                marginTop: "20px",

                paddingLeft: "25px",
                marginRight: "10px",
                paddingRight: "25px",
              }}
              type="button"
              onClick={handleSubmit}
            >
              {load ? <Spin size="small" className="mr-2" /> : ""}
              {t("Save")}
            </button>
          </Row>
        </Form>
      </PanelBody>
    </Panel>
  );
}
export default withTranslation()(AddOperator);
