import React from "react";
import namor from "namor";
import ReactTable from "react-table";
import { Spin,Empty } from "antd";
import { Link, Redirect } from "react-router-dom";
import { Panel, PanelHeader } from "../../components/panel/panel.jsx";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { withRouter } from "react-router-dom";
import url from "../../url.json";
import { useTranslation, withTranslation } from "react-i18next";
import axios from "axios";
import dateFormat from "dateformat";
import { toast } from "react-toastify";
class Operators extends React.Component {
  constructor(props) {
    super(props);
    this.operators = [];

    this.state = {
      operators: [],
      load: false,
      ids: 0,
      t: false,
      iddelite: "",
      title: "",
      modalDialog: false,
      modalWithoutAnimation: false,
      modalMessage: false,
      modalAlert: false,
    };

    this.toggleModal = this.toggleModal.bind(this);
  }

  componentDidMount() {
    axios({
      url: url.url + "auth/getAll?search=2",
    })
      .then((response) => {
        this.setState({ operators: response.data.data });
      })
      .catch(function (response) {
        console.log(response);
      });
  }
  handledelite() {
    this.setState({ load: false });
    const delstate = this.state.operators;
    delstate.splice(this.state.ids, 1);
    this.setState({ operators: delstate });
    axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${localStorage.getItem("token")}`;

    axios
      .put(url.url + `auth/disable?userId=${this.state.iddelite}`)
      .then(function (response) {
        if (response.data.success) {

          toast.success("Muaffaqiyatli o'chirildi!");
        }
      })
      .catch(function (response) {
        console.log(response);
      });
  }
  toggleModal(name) {
    switch (name) {
      case "modalDialog":
        this.setState({ modalDialog: !this.state.modalDialog });
        break;
      case "modalWithoutAnimation":
        this.setState({
          modalWithoutAnimation: !this.state.modalWithoutAnimation,
        });
        break;
      case "modalMessage":
        this.setState({ modalMessage: !this.state.modalMessage });
        break;
      case "modalAlert":
        this.setState({ modalAlert: !this.state.modalAlert });
        break;
      default:
        break;
    }
  }

  render() {
    const { t } = this.props;
    return (
      <div>
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link to="/home">{t("Home")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/card">{t("Products")}</Link>
          </li>{" "}
          <li className="breadcrumb-item">
            <Link to="/category">{t("Category")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/operators">{t("Operators")}</Link>
          </li>
          <li className="breadcrumb-item">
            <Link to="/users">{t("Users")}</Link>
          </li>
        </ol>
        <h1 className="page-header">{t("Operators")}</h1>

        <Modal
          isOpen={this.state.modalAlert}
          toggle={() => this.toggleModal("modalAlert")}
        >
          <ModalHeader toggle={() => this.toggleModal("modalAlert")}>
            {t("Delete")}
          </ModalHeader>
          <ModalBody>
            <div className="alert alert-danger m-b-0">
              <h5>
                <i className="fa fa-info-circle"></i> {this.state.title}
              </h5>
              <p></p>
            </div>
          </ModalBody>
          <ModalFooter>
            <button
              className="btn btn-white"
              onClick={() => this.toggleModal("modalAlert")}
            >
              {t("Cancel")}
            </button>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.toggleModal("modalAlert");
                this.handledelite();
              }}
            >
              {t("Delete")}
            </button>
          </ModalFooter>
        </Modal>

        <Panel>
          <PanelHeader>{t("Operators list")}</PanelHeader>
          <div style={{ textAlign: "right", marginRight: "10px" }}>
            <button
              className="btn btn-primary fileinput-button m-r-3"
              style={{
                marginTop: "5px",
                marginRight: "0px",
                marginLeft: "avto",
              }}
              onClick={() => this.props.history.push("/addoperator")}
            >
              <i className="fa fa-fw fa-plus"></i>
              {t("Add")}..
            </button>
          </div>
          <div className="panel-body">
          {this.state.operators.length>0?
            <div className="table-responsive">
              <table className="table table-striped m-b-0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>{t("Fullname")}</th>
                    <th>{t("Username")}</th>
                    <th>{t("Created at")}</th>

                    <th width="1%"></th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.operators?.map((res, i) => (
                    <tr key={i}>
                      <td className="with-img">{i + 1}</td>
                      <td>{res.username}</td>
                      <td>{res.username}</td>
                      <td>{dateFormat(res.createdAt, "dd.mm.yyyy")}</td>

                      <td className="with-btn">

                        <button
                          onClick={() => {
                            this.toggleModal("modalAlert");
                            this.setState({ iddelite: res.id });
                            this.setState({ title: res.username });
                            this.setState({ ids: i });
                          }}
                          className="btn btn-sm btn-danger "
                        >
                          <i className="fas fa-trash-alt"></i>
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>:
            <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            minHeight: 500,
            backgroundColor: "white",
            borderRadius: 12,
          }}
        >
            <Empty description={t("No data")} />
        </div>}

          </div>
        </Panel>

      </div>
    );
  }
}

export default withTranslation()(withRouter(Operators));
