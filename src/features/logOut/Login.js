/* eslint-disable jsx-a11y/alt-text */
import { error } from "jquery";
import React from "react";
import { withRouter } from "react-router-dom";
import { PageSettings } from "../../config/page-settings.js";
import url from "../../url.json";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import Password from "antd/lib/input/Password.js";
import { useTranslation } from "react-i18next";
class Login extends React.Component {
  static contextType = PageSettings;
  constructor(props) {
    super(props);
    this.state = {
      res: [],
      error: "",
      load: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.context.handleSetPageSidebar(false);
    this.context.handleSetPageHeader(false);
    this.context.handleSetBodyWhiteBg(true);
    localStorage.removeItem("operator")
  }
  componentDidUpdate(prevProps) {
    const { isNotificationOpen } = this.props;

    if (prevProps.isNotificationOpen !== isNotificationOpen) {
      isNotificationOpen && this.notify();
    }
  }
  componentWillUnmount() {
    this.context.handleSetPageSidebar(true);
    this.context.handleSetPageHeader(true);
    this.context.handleSetBodyWhiteBg(false);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ load: true });
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    if (username == "") {
      this.setState({ error: "Username maydoni bo'sh bo'lishi mumkin emas!" });
    }
    if (password == "") {
      this.setState({ error: " Password maydoni bo'sh bo'lishi mumkin emas!" });
    }
    if (username != "" && password != "") {
      const data = {
        username: username,
        password: password,
      };
      const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(data),
      };

      fetch(url.url + "auth/login", requestOptions)
        .then((response) => response.json())
        .then((data) => {
          if (data.success) {
            toast.success("Muvaffaqiyatli bajarildi !");
            if (data.message == "SYSTEM_ROLE_OPERATOR") {
              localStorage.setItem("operator", data.data);
              this.setState({ loading: false });
              setTimeout(() => {
                this.props.history.push("/sellers");
              }, 1000);
            } else {
              if (data.message == "SYSTEM_ROLE_ADMIN") {
                localStorage.setItem("token", data.data);

                this.setState({ loading: false });
                setTimeout(() => {
                  this.props.history.push("/home");
                }, 1000);
              } else {
                toast.error("Ruxsat yoq!");
              }
            }
          } else this.setState({ error: data.message });
          this.setState({
            res: data,
          });
        })
        .catch((error) => console.log(error));
    }
  }

  render() {
    const { t } = this.props;

    return (
      <div className="login login-with-news-feed">
        {/* <!-- begin news-feed --> */}
        <div className="news-feed">
          <div
            className="news-image"
            style={{
              backgroundImage: "url(../assets/img/login-bg/login-bg-11.jpg)",
            }}
          ></div>
          <div className="news-caption">
            <h4 className="caption-title">
              <b>Libazar</b>
            </h4>
          </div>
        </div>
        <div className="right-content">
          <div className="login-header">
            <div className="brand">
              <b> Libazar</b>
              <small>...</small>
            </div>
            <div className="icon">
              <i className="fa fa-sign-in-alt"></i>
            </div>
          </div>

          <div className="login-content">
            <div className="form-group m-b-15">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="User Name"
                id="username"
                name="username"
                required
              />
            </div>
            <div className="form-group m-b-15">
              <input
                type="password"
                className="form-control form-control-lg"
                placeholder="Password"
                id="password"
                name="password"
                required
              />
            </div>
            <p style={{ color: "red" }}>
              {this.state.error ? this.state.error : ""}
            </p>
            <div className="login-buttons">
              <button
                type="submit"
                className="btn btn-success btn-block btn-lg"
                onClick={this.handleSubmit}
              >
                Sign me in {this.state.load ? "..." : ""}
              </button>
            </div>

            <hr />
            <p className="text-center text-grey-darker mb-0">
              &copy;Libazar 2022
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
